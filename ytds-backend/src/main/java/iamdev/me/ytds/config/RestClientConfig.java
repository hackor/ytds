package iamdev.me.ytds.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.lang.NonNull;

@Configuration
public class RestClientConfig extends AbstractElasticsearchConfiguration {

    @Autowired
    private EsConfig esConfig;

    @Override
    @Bean
    @NonNull
    public RestHighLevelClient elasticsearchClient() {
        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
            .connectedTo(esConfig.getIp() + ":" + esConfig.getPort())
            .build();
        return RestClients.create(clientConfiguration).rest();
    }
}
