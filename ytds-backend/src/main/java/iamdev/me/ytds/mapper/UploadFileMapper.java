package iamdev.me.ytds.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import iamdev.me.ytds.entity.UploadFile;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author zxc
 * @since 2018-05-09
 */
public interface UploadFileMapper extends BaseMapper<UploadFile> {

}
