package iamdev.me.ytds.service;

import com.baomidou.mybatisplus.extension.service.IService;
import iamdev.me.ytds.entity.DocDraft;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
public interface IDocDraftService extends IService<DocDraft> {

}
