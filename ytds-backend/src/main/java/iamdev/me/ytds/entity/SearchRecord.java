package iamdev.me.ytds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@TableName("tb_search_record")
public class SearchRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 搜索id
     */
    @TableId(value = "search_record_id", type = IdType.AUTO)
    private Integer searchRecordId;
    /**
     * 搜索关键词
     */
    @TableField("search_keyword")
    private String searchKeyword;
    /**
     * 搜索后阅读了哪篇文档
     */
    @TableField("doc_id")
    private Integer docId;
    /**
     * 搜索人
     */
    @TableField("user_id")
    private Integer userId;


    public Integer getSearchRecordId() {
        return searchRecordId;
    }

    public void setSearchRecordId(Integer searchRecordId) {
        this.searchRecordId = searchRecordId;
    }

    public String getSearchKeyword() {
        return searchKeyword;
    }

    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "SearchRecord{" +
            ", searchRecordId=" + searchRecordId +
            ", searchKeyword=" + searchKeyword +
            ", docId=" + docId +
            ", userId=" + userId +
            "}";
    }
}
