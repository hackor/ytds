package iamdev.me.ytds.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Mapping;
import org.springframework.data.elasticsearch.annotations.Setting;

/**
 * <p>
 *
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@TableName("tb_doc")
@Document(indexName = "doc")
@Setting(
    replicas = 0, refreshInterval = "5s"
)
@Mapping(mappingPath = "/es/doc_mappings.json")
public class Doc implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @TableField(exist = false)
    private String id;
    @TableId(value = "doc_id", type = IdType.AUTO)
    private Integer docId;
    @TableField("doc_name")
    private String docName;
    @TableField("doc_size")
    private Long docSize;
    @TableField("doc_sha256")
    private String docSha256;
    @TableField("doc_create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date docCreateDate;
    @TableField("doc_user_id")
    private Integer docUserId;
    @TableField("doc_file_id")
    private String docFileId;
    @TableField("doc_open")
    private Integer docOpen;
    @TableField("doc_type")
    private String docType;
    @TableField("doc_title")
    private String docTitle;
    @TableField("doc_content")
    private String docContent;
    @TableField("doc_delete")
    private Integer docDelete;

    //文档是否被索引了
    @TableField("doc_index")
    private Integer docIndex;

    @TableField(exist = false)
    private List<String> docContentHighLightList;

    @TableField(exist = false)
    private List<String> docTitleHighLightList;
    @TableField("doc_modify_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", locale = "zh", timezone = "GMT+8")
    private Date docModifyDate;
    @TableField("source")
    private String source;
    @TableField("source_url")
    private String sourceUrl;
    //文档状态
    @TableField("doc_status")
    //0表示等待索引，1表示索引成功，2表示索引失败
    private Integer docStatus;

    @TableField("doc_convert")
    //0表示等待转换，1表示转换成功，2表示转换失败
    private Integer docConvert;

    public Integer getDocConvert() {
        return docConvert;
    }

    public void setDocConvert(Integer docConvert) {
        this.docConvert = docConvert;
    }

    public Integer getDocStatus() {
        return docStatus;
    }

    public void setDocStatus(Integer docStatus) {
        this.docStatus = docStatus;
    }

    public String getDocFileId() {
        return docFileId;
    }

    public void setDocFileId(String docFileId) {
        this.docFileId = docFileId;
    }

    public Integer getDocIndex() {
        return docIndex;
    }

    public void setDocIndex(Integer docIndex) {
        this.docIndex = docIndex;
    }

    public List<String> getDocContentHighLightList() {
        return docContentHighLightList;
    }

    public void setDocContentHighLightList(List<String> docContentHighLightList) {
        this.docContentHighLightList = docContentHighLightList;
    }

    public List<String> getDocTitleHighLightList() {
        return docTitleHighLightList;
    }

    public void setDocTitleHighLightList(List<String> docTitleHighLightList) {
        this.docTitleHighLightList = docTitleHighLightList;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public String getDocName() {
        return docName;
    }

    public void setDocName(String docName) {
        this.docName = docName;
    }

    public Long getDocSize() {
        return docSize;
    }

    public void setDocSize(Long docSize) {
        this.docSize = docSize;
    }

    public String getDocSha256() {
        return docSha256;
    }

    public void setDocSha256(String docSha256) {
        this.docSha256 = docSha256;
    }

    public Date getDocCreateDate() {
        return docCreateDate;
    }

    public void setDocCreateDate(Date docCreateDate) {
        this.docCreateDate = docCreateDate;
    }

    public Integer getDocUserId() {
        return docUserId;
    }

    public void setDocUserId(Integer docUserId) {
        this.docUserId = docUserId;
    }


    public Integer getDocOpen() {
        return docOpen;
    }

    public void setDocOpen(Integer docOpen) {
        this.docOpen = docOpen;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocTitle() {
        return docTitle;
    }

    public void setDocTitle(String docTitle) {
        this.docTitle = docTitle;
    }

    public String getDocContent() {
        return docContent;
    }

    public void setDocContent(String docContent) {
        this.docContent = docContent;
    }

    public Integer getDocDelete() {
        return docDelete;
    }

    public void setDocDelete(Integer docDelete) {
        this.docDelete = docDelete;
    }

    public Date getDocModifyDate() {
        return docModifyDate;
    }

    public void setDocModifyDate(Date docModifyDate) {
        this.docModifyDate = docModifyDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public void setSourceUrl(String sourceUrl) {
        this.sourceUrl = sourceUrl;
    }

    @Override
    public String toString() {
        return "Doc{" +
            ", docId=" + docId +
            ", docName=" + docName +
            ", docSize=" + docSize +
            ", docSha256=" + docSha256 +
            ", docCreateDate=" + docCreateDate +
            ", docUserId=" + docUserId +
            ", docOpen=" + docOpen +
            ", docType=" + docType +
            ", docTitle=" + docTitle +
            ", docContent=" + docContent +
            ", docDelete=" + docDelete +
            ", docModifyDate=" + docModifyDate +
            "}";
    }
}
