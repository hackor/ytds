package iamdev.me.ytds.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zxc
 * @since 2018-06-22
 */
@TableName("tb_download_record")
public class DownloadRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 下载id
     */
    @TableId(value = "download_id", type = IdType.AUTO)
    private Integer downloadId;
    /**
     * 下载人
     */
    @TableField("download_user_id")
    private Integer downloadUserId;
    /**
     * 下载日期
     */
    @TableField("download_date")
    private Date downloadDate;
    /**
     * 下载的文档id
     */
    @TableField("doc_id")
    private Integer docId;


    public Integer getDownloadId() {
        return downloadId;
    }

    public void setDownloadId(Integer downloadId) {
        this.downloadId = downloadId;
    }

    public Integer getDownloadUserId() {
        return downloadUserId;
    }

    public void setDownloadUserId(Integer downloadUserId) {
        this.downloadUserId = downloadUserId;
    }

    public Date getDownloadDate() {
        return downloadDate;
    }

    public void setDownloadDate(Date downloadDate) {
        this.downloadDate = downloadDate;
    }

    public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    @Override
    public String toString() {
        return "DownloadRecord{" +
            ", downloadId=" + downloadId +
            ", downloadUserId=" + downloadUserId +
            ", downloadDate=" + downloadDate +
            ", docId=" + docId +
            "}";
    }
}
